import { Tooltip } from 'antd';
import { LogoutOutlined } from '@ant-design/icons';

const Logout = ( props ) => {
  const { token, setToken } = props;
  
  const onHandleLogOut = async () => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_API}/logout/`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    })

    const data = await res.json();
    setToken(data);
  }

  return (
    <Tooltip placement="topLeft" title="Log Out" >
      <LogoutOutlined onClick={onHandleLogOut} />
    </Tooltip>
  )
}

export default Logout;
