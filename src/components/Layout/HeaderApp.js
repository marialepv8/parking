import './HeaderApp.css';
import { Layout, Row, Col } from 'antd';
import Logout from '../Logout/Logout';
import { Link } from 'react-router-dom';
const { Header } = Layout;

const HeaderApp = ( props ) => {
  const { token, setToken } = props;

  return (
    <Layout>
      <Header className="header">
        <Row gutter={4}>
            <Col span={4}>
              <div className="logo"></div>
            </Col>
            <Col span={20}>
              {
                token &&
                <Row gutter={12} className="menu-right ">
                  <Col>
                    <Link to='/dashboard'>Dashboard</Link>
                  </Col>
                  <Col>
                    <Link to='/tickets'>Tickets</Link>
                  </Col>
                  <Col>
                    <Logout token={token} setToken={setToken} />
                  </Col>
                </Row>
              }
            </Col>
          </Row>
      </Header>
    </Layout>
  )
}

export default HeaderApp;
