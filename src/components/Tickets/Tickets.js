import './Tickets.css';
import { Table } from 'antd';
import { useEffect, useState } from 'react';

const columns = [
    {
      title: 'Placa',
      dataIndex: 'placa',
      key: 'placa',
    },
    {
      title: 'Entrada',
      dataIndex: 'entrada',
      key: 'entrada',
    },
    {
      title: 'Salida',
      dataIndex: 'salida',
      key: 'salida',
    },
    {
      title: 'Estado Tickect',
      dataIndex: 'estado_ticket',
      key: 'estado_ticket',
    },
    {
      title: 'Estado Pago',
      dataIndex: 'estado_pago',
      key: 'estado_pago',
    },
];

const Tickets = ( props ) => {
    const { token } = props;
    const [ tickets, setTickets ] = useState([]);

    useEffect(() => {
        getTickets();
    }, []);

    const getTickets = async () => {
        const res = await fetch(`${process.env.REACT_APP_BACKEND_API}/tickets/`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })

        const data = await res.json();
        setTickets(data);
    }

    return (
        <div>
            <Table dataSource={tickets} rowKey="id" columns={columns} />
        </div>
    )
}

export default Tickets;