import './App.css';
import React, { useEffect } from 'react';
import { Route, Switch, useHistory } from 'react-router-dom';
import useToken from './useToken';
import Layout from '../Layout/LayoutApp';
import Login from '../Login/Login';
import Register from '../Register/Register';
import Dashboard from '../Dashboard/Dashboard';
import Tickets from '../Tickets/Tickets';

const App = () => {
  const { token, setToken } = useToken();
  const history = useHistory();

  useEffect(() => {
    if(!token) {
      history.push('/');
      localStorage.clear();
    }
  })

  return (
    <Layout token={token} setToken={setToken} >
      <Switch>
        {
          !token ?
          (
            <React.Fragment>
              <Route exact path="/" component={Login}>
                <Login setToken={setToken} />
              </Route>
              <Route exact path="/register" component={Register}>
                <Register />
              </Route>
            </React.Fragment>
          )
          :
          (
            <React.Fragment>
              <Route exact path="/dashboard" component={Dashboard}>
                <Dashboard token={token} />
              </Route>
              <Route exact path="/tickets" component={Tickets}>
                <Tickets token={token} />
              </Route>
            </React.Fragment>
          )
        }
      </Switch>
    </Layout>
  )
}

export default App;
