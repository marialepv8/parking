import './Login.css';
import { Form, Input, Button, notification } from 'antd';
import { UserOutlined, LockOutlined, LoginOutlined } from '@ant-design/icons';
import { Link, useHistory } from 'react-router-dom';

const Login = ( props ) => {
  const history = useHistory();
  const initialValues = {
    username: '',
    password: ''
  };

  const singIn = async ( credentials ) => {
    const res = await fetch(`${process.env.REACT_APP_BACKEND_API}/login/`, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify( credentials )
    });

    const data = await res.json();
    if( data && data.access ) {
      props.setToken(data);
      history.push('/dashboard');
    } else {
      notification.info ({
        message: 'Notification Login',
        description: `Username and Password are not registered. ${ data.detail }`, 
        placement: 'bottomRight'
      })
    }
  }
  
  const onFinish = ( values ) => {
    singIn(values);
  };

  return (
    <div>
      <h1 className="text-align-center animate__animated animate__backInUp">Login</h1>

      <Form
        name="formSignInLogin"
        initialValues={ initialValues }
        onFinish={ onFinish }
        className="align-content-center animate__animated animate__backInLeft animate__delay-1s"
      >
        <Form.Item name="username" rules={[{ required: true, message: 'Username is required!' }]} >
          <Input prefix={ <UserOutlined /> } type="text" placeholder="Username" />
        </Form.Item>
        <Form.Item name="password" rules={[{ required: true, message: 'Password is required!' }]} >
          <Input prefix={ <LockOutlined /> } type="password" placeholder="Password" />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Log in <LoginOutlined />
          </Button>
        </Form.Item>
        <Link to='/register'>Register Now!</Link>
      </Form>
    </div>
  );
}

export default Login;